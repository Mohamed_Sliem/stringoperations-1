package org.eghagha.csce;

public interface Strategy {
	   public void doOperation();
}
